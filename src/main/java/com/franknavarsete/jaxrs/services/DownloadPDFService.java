package com.franknavarsete.jaxrs.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.File;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: Frank
 * Date: 27.11.13
 * Time: 22:48
 */
@Path("/pdf")
public class DownloadPDFService {

    private static final String FILE_PATH = "/Users/Frank/Documents/Dev/Java/jaxrs/src/main/resources/testpdf.pdf";


    /**
     * URI Pattern : /pdf/get
     * @return
     */
    @GET
    @Path("/get")
    @Produces("application/pdf")
    public Response getFile() {

        File file = new File(FILE_PATH);

        Response.ResponseBuilder response = Response.ok((Object) file);
        response.header("Content-Disposition",
                "attachment; filename=new-android-book.pdf");
        return response.build();

    }
}
