package com.franknavarsete.jaxrs.services;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.HttpHeaders;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: Frank
 * Date: 27.11.13
 * Time: 21:44
 */
@Path("HTTPHeader")
public class HTTPHeaderService {

    /**
     * URI Pattern : main/HTTPHeader/get
     * @param userAgent
     * @return
     */
    @GET
    @Path("/get")
    public Response getHeaderInformation(@HeaderParam("user-agent") String userAgent) {
        return Response.status(200)
                .entity("addUser is called, userAgent : " + userAgent)
                .build();
    }

    /**
     * URI Pattern : main/HTTPHeader/getContext
     * @param headers
     * @return
     */
    @GET
    @Path("/getContext")
    public Response getContextInformation(@Context HttpHeaders headers) {
        String userAgent = headers.getRequestHeader("user-agent").get(0);

        //List all request headers
        for(String header : headers.getRequestHeaders().keySet()){
            System.out.println(header);
        }

        return Response.status(200)
                .entity("addUser is called, userAgent : " + userAgent)
                .build();
    }
}
