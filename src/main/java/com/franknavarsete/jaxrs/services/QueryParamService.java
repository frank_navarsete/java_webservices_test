package com.franknavarsete.jaxrs.services;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: Frank
 * Date: 27.11.13
 * Time: 14:27
 */
@Path("queryParam")
public class QueryParamService {

    /**
     * URI Pattern : “queryParam/query?from=100&to=200&orderBy=age&orderBy=name”
     * @param from
     * @param to
     * @param orderBy
     * @return
     */
    @GET
    @Path("/query")
    public Response getUser(
                @QueryParam("from") int from,
                @QueryParam("to") int to,
                @QueryParam("orderBy")List<String> orderBy) {

        return Response
                .status(200)
                .entity("getUsers is called, from : " + from + ", to : " + to
                        + ", orderBy" + orderBy.toString()).build();
    }

    /**
     * URI Pattern: /queryContext?from=100&to=200&orderBy=age&orderBy=name
     * @param info
     * @return
     */
    @GET
    @Path("/queryContext")
    public Response getUsers(@Context UriInfo info) {

        String from = info.getQueryParameters().getFirst("from");
        String to = info.getQueryParameters().getFirst("to");
        List<String> orderBy = info.getQueryParameters().get("orderBy");

        return Response
                .status(200)
                .entity("getUsers is called, from : " + from + ", to : " + to
                        + ", orderBy" + orderBy.toString()).build();

    }

    /**
     * URI Pattern: /queryDefault
     * @param from
     * @param to
     * @param orderBy
     * @return
     */
    @GET
    @Path("/queryDefault")
    public Response getUsersDefault(
            @DefaultValue("1000") @QueryParam("from") int from,
            @DefaultValue("999")@QueryParam("to") int to,
            @DefaultValue("name") @QueryParam("orderBy") List<String> orderBy) {

        return Response
                .status(200)
                .entity("getUsers is called, from : " + from + ", to : " + to
                        + ", orderBy" + orderBy.toString()).build();
    }
}
