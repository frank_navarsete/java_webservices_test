package com.franknavarsete.jaxrs.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: Frank
 * Date: 26.11.13
 * Time: 22:08
 */
@Path("/regex")
public class RegexService {

    @GET
    public Response standardResponse() {
        String output = "Add /users/<username>" +
                        " or /books/<isbn>(digit)";

        return Response.status(200).entity(output).build();
    }

    @GET
    @Path("{id: \\d+}") //Support only digits
    public Response userIDResponseService(@PathParam("id") String id) {
        return Response.status(200).entity("getUserById is called, id : " + id).build();
    }

    @GET
    @Path("/books/{isbn : \\d+}")
    public Response getUserBookByISBN(@PathParam("isbn") String isbn) {

        return Response.status(200)
                .entity("getUserBookByISBN is called, isbn : " + isbn).build();

    }

    @GET
    @Path("/username/{username : [a-zA-Z][a-zA-Z_0-9]}")
    public Response getUserByUserName(@PathParam("username") String username) {

        return Response.status(200)
                .entity("getUserByUserName is called, username : " + username).build();

    }
}
