package com.franknavarsete.jaxrs.services;

import com.franknavarsete.jaxrs.services.xml.Customer;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: Frank
 * Date: 28.11.13
 * Time: 22:37
 */
@Path("xml/customer")
public class XMLService {

    /**
     * URI Pattern : main/xml/customer/125
     *
     * Return
     *  <customer pin="125">
     *      <name>Frank</name>
     *  </customer>
     * @param pin
     * @return
     */
    @GET
    @Path("/{pin}")
    @Produces(MediaType.APPLICATION_XML)
    public Customer getCustomerInXML(@PathParam("pin") int pin) {
        Customer customer = new Customer();
        customer.setName("Frank");
        customer.setPin(pin);

        return customer;
    }
}
