package com.franknavarsete.jaxrs.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: Frank
 * Date: 27.11.13
 * Time: 14:22
 */
@Path("multiParam")
public class MultipleParamService {

    @GET
    @Path("{year}/{month}/{day}")
    public Response getUserHistory(
                        @PathParam("year") int year,
                        @PathParam("month") int month,
                        @PathParam("day") int day) {
        String date = year + "/" + month + "/" + day;

        return Response.status(200)
                .entity("getUserHistory is called, year/month/day : " + date)
                .build();
    }
}
