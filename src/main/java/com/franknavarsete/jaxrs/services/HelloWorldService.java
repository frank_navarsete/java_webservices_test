package com.franknavarsete.jaxrs.services;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: Frank
 * Date: 23.11.13
 * Time: 15:47
 */
@Path("greetings")
public class HelloWorldService {


    //Service with no parameter(Json Object)
    @GET
    public JsonObject greetings() {
        return Json.createObjectBuilder().add("age", 26)
                                        .add("name", "Frank")
                                        .add("location", "Elverum")
                                    .build();
    }

    //Service with parameter (Response)
    @GET
    @Path("/{param}")
    public Response getMsg(@PathParam("param") String msg) {
        String output = "Say hello to my new service, " + msg + ".";

        return Response.status(200).entity(output).build();
    }
}
