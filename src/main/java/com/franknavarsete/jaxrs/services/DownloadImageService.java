package com.franknavarsete.jaxrs.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.File;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: Frank
 * Date: 27.11.13
 * Time: 22:27
 */
@Path("/images")
public class DownloadImageService {

    private static final String FILE_PATH = "/Users/Frank/Documents/Dev/Java/jaxrs/src/main/resources/image.jpeg";

    /**
     * URI Pattern : /images/get
     * @return
     */
    @GET
    @Path("/get")
    @Produces("image/jpeg")
    public Response getFile() {

        File file = new File(FILE_PATH);

        Response.ResponseBuilder response = Response.ok((Object) file);
        response.header("Content-Disposition",
                "attachment; filename=image_from_server.jpeg");
        return response.build();

    }
}
