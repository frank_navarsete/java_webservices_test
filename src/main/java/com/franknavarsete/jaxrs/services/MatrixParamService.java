package com.franknavarsete.jaxrs.services;

import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: Frank
 * Date: 27.11.13
 * Time: 17:06
 */
@Path("Matrix/books")
public class MatrixParamService {


    /**
     * URI Pattern: Matrix/books/2011/
     *              Matrix/books/2011;author=frank
     *              Matrix/books/2011;author=frank;country=norway
     *              Matrix/books/2011;country=norway;author=frank
     * @param year
     * @param author
     * @param country
     * @return
     */
    @GET
    @Path("{year}")
    public Response getBooks(@PathParam("year") String year,
                             @MatrixParam("author") String author,
                             @MatrixParam("country") String country) {
        return Response
                .status(200)
                .entity("getBooks is called, year : " + year
                        + ", author : " + author + ", country : " + country)
                .build();
    }
}
