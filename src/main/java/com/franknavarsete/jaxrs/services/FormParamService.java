package com.franknavarsete.jaxrs.services;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: Frank
 * Date: 27.11.13
 * Time: 18:04
 */
@Path("User")
public class FormParamService {

    /**
     * URL : http://localhost:8080/<applicationName>/UserForm.html
     * @param name
     * @param age
     * @return
     */
    @POST
    @Path("/add")
    public Response addUser(
            @FormParam("name") String name,
            @FormParam("age") int age) {

        return Response.status(200)
                .entity("addUser is called, name : " + name + ", age : " + age)
                .build();

    }
}
