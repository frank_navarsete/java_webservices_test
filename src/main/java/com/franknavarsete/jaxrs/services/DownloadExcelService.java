package com.franknavarsete.jaxrs.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.File;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: Frank
 * Date: 27.11.13
 * Time: 22:59
 */
@Path("/excel")
public class DownloadExcelService {

    private static final String FILE_PATH = "/Users/Frank/Documents/Dev/Java/jaxrs/src/main/resources/TestExcel.xls";

    @GET
    @Path("/get")
    @Produces("application/vnd.ms-excel")
    public Response getFile() {

        File file = new File(FILE_PATH);

        Response.ResponseBuilder response = Response.ok((Object) file);
        response.header("Content-Disposition",
                "attachment; filename=new-excel-file.xls");
        return response.build();

    }
}
