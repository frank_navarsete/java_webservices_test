package com.franknavarsete.jaxrs.services;

import java.io.File;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: Frank
 * Date: 27.11.13
 * Time: 22:09
 */
@Path("/file")
public class DownloadTextFileService {

    private static final String FILE_PATH = "/Users/Frank/Documents/Dev/Java/jaxrs/src/main/resources/TextFile.txt";

    /**
     * URI Pattern: main/file/get
     * @return
     */
    @GET
    @Path("/get")
    @Produces("text/plain")
    public Response getFile() {

        File file = new File(FILE_PATH);

        ResponseBuilder response = Response.ok((Object) file);
        response.header("Content-Disposition",
                "attachment; filename=\"file_from_server.log\"");
        return response.build();

    }
}
