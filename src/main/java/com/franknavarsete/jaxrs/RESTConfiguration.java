package com.franknavarsete.jaxrs;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: Frank
 * Date: 23.11.13
 * Time: 15:46
 */
@ApplicationPath("main")
public class RESTConfiguration extends Application {
}
